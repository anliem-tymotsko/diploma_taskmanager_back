package com.diploma.version1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@SpringBootApplication
public class Version1Application {

    public static void main(String[] args) {
        SpringApplication.run(Version1Application.class, args);
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/task_manager","dispatch_alf","Tcms@2018!@#");
            Statement stmt=con.createStatement();

            con.close();
        }catch(Exception e){ System.out.println(e);}
    }


}
